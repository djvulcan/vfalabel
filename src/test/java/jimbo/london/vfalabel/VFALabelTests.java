package jimbo.london.vfalabel;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


class VFALabelTests {

    @Test
    void contextLoads() {
    }

    @Test
    void FuntionalTest () {
        VFALabel testLable = new VFALabel("jaws", "11", "22");
        assertEquals(testLable.Label, "J57S1122");
        VFALabel testLable2 = new VFALabel("the hobbit: an unexpected journey", "33", "44");
        assertEquals(testLable2.Label, "T51J3344");
        VFALabel testLable3 = new VFALabel("oh, god!", "55", "66");
        assertEquals(testLable3.Label, "O57R5566");
    }

}
