package jimbo.london.vfalabel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class VFALabelUnitTests {
    public Movie testJaws = new Movie();

    @Test
    void testProcessMovieIntoMovieData(){
        testJaws.setDirector("Gregg Turkington");
        testJaws.setYear("2010");
        testJaws.setMetascore("100");
        testJaws.setTitle("Decker");
        MovieData testMovieData = mock(MovieData.class);
        testMovieData.movieData = testJaws;
        when(testMovieData.findMovie()).thenReturn(true);
        testMovieData.findMovie();
        assertEquals(testMovieData.movieData.Title,"Decker");
    }
/*
    void testProcessMovieDataIntoLabel(){
        testJaws.setDirector("Gregg Turkington");
        testJaws.setYear("2010");
        testJaws.setMetascore("100");
        testJaws.setTitle("Decker");
        MovieData testVFALabel = mock(VFALabel.class);
        VFALabel.movieData = testJaws;
        when(testMovieData.findMovie()).thenReturn(true);
        testMovieData.findMovie();
        assertEquals(testMovieData.movieData.Title,"Decker");
    }

 */
}
