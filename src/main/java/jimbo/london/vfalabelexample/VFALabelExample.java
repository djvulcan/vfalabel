package jimbo.london.vfalabelexample;

import jimbo.london.vfalabel.VFALabel;

import java.util.Scanner;

public class VFALabelExample {
    public static void main(String[] args) {
        String runExample = "y";
        do {
            Scanner input = new Scanner(System.in);
            System.out.println("Input Movie Title: ");
            String movieChoice = input.nextLine();
            System.out.println("Input Movie Shelf No: ");
            String movieShelf = input.next();
            System.out.println("Input Movie Tape No: ");
            String movieTape = input.next();
            String movieLabel = new VFALabel(movieChoice, movieShelf, movieTape).Label;
            System.out.println(String.format("Your Victorville Coded label for %s is %s.", movieChoice, movieLabel));
            System.out.println("\n Search another Movie? y/n: ");
            runExample = input.next();
        }
        while (runExample.toLowerCase().equals("y"));
    }
}
