package jimbo.london.vfalabel;

import org.springframework.web.client.RestTemplate;
public class VFALabel {
    String movieTitle;
    String movieShelf;
    String movieTape;
    public String Label;

    public VFALabel(String movieTitle, String movieShelf, String movieTape) {
        this.movieTitle = movieTitle;
        this.movieShelf = movieShelf;
        this.movieTape = movieTape;
        this.generateLabel();
    }

    private void generateLabel() {
        MovieData movieData = new MovieData(movieTitle);
        String movieTitleInitial = getMovieTitleInitial(movieTitle);
        String directorInitialLabel = directorInitial(movieData.movieData.Director);
        System.out.println(String.format("Director Initial: %s.", directorInitialLabel));
        String movieDecadeLabel = getDecade(movieData.movieData.Year);
        System.out.println(String.format("Decade: %s.", movieDecadeLabel));
        String popcornBagsLabel = getPopcornRating(movieData.movieData.Metascore);
        System.out.println(String.format("Popcorn Rating: %s bags.", popcornBagsLabel));
        Label = String.format("%s%s%s%s%s%s", movieTitleInitial, popcornBagsLabel, movieDecadeLabel,
                directorInitialLabel, movieShelf, movieTape);
    }

    private String directorInitial(String directorName) {
        String lastName;
        int arrayLength = directorName.lastIndexOf(' ');
        if (arrayLength == -1) {
            lastName = directorName;
        } else {
            lastName = directorName.substring(arrayLength + 1);
        }
        String firstLetter = Character.toString(lastName.charAt(0));
        return firstLetter.toUpperCase();
    }

    private String getDecade(String movieYear) {
        return Character.toString(movieYear.charAt(2));
    }

    private String getMovieTitleInitial(String movieTitle) {
        return Character.toString(movieTitle.charAt(0)).toUpperCase();
    }

    private String getPopcornRating(String imdbRating) {
        String popcornBags;
        int ratingint = Integer.parseInt(Character.toString(imdbRating.charAt(0)));
        if (ratingint > 5) {
            popcornBags = "5";
        } else {
            popcornBags = String.format("%d", ratingint);
        }
        return popcornBags;
    }


}

class MovieData {
    private final String searchTitle;
    public Movie movieData;

    public MovieData(String searchTitle) {
        this.searchTitle = searchTitle;
        if (findMovie()) {
            parseMovieData();
        }
    }

    public boolean findMovie() {
        System.out.println(String.format("Searching OMDB for %s.", searchTitle));
        try {
            String apiKey = "895d0c29";
            String baseURL = "http://omdbapi.com";
            String fullURL = String.format("%s/?apikey=%s&t=%s", baseURL, apiKey, searchTitle);
            RestTemplate template = new RestTemplate();
            movieData = template.getForObject(fullURL, Movie.class);
            return true;
        } catch (Exception e) {
            System.out.println("HACKED - can't call OMDb API-" + e.getMessage());
            return false;
        }
    }

    public void parseMovieData() {
        System.out.println(String.format("Full Title: %s.", movieData.Title));
        System.out.println(String.format("Director: %s.", movieData.Director));
        System.out.println(String.format("Year: %s.", movieData.Year));
        System.out.println(String.format("Movie Rating: %s.", movieData.Metascore));
    }
}


