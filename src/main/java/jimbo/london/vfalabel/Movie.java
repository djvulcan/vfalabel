package jimbo.london.vfalabel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie {
    public String Title;
    public String Year;
    public String Director;
    public String Metascore;

    public Movie() {
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String director) {
        Director = director;
    }

    public String getMetascore() {
        return Metascore;
    }

    public void setMetascore(String metascore) {
        Metascore = metascore;
    }

    @Override
    public String toString() {
        return String.format("{Title: \"%s\", Year: \"%s\", Director: \"%s\", Metascore\"%s\"}",
                Title, Year, Director, Metascore);
    }
}
